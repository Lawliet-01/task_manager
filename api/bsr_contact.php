<?php
//This API is for the BSR App Client Registration
//All data will come in base64 format

include_once 'Util.php';
include_once './../../mail/mailer.php';

$status = 1;
$message = "";
$user_detail = array();

if ($_SERVER['REQUEST_METHOD'] != "POST") {
    Util::sendResponse(0, 'Invalid method to access to this API. This API is only for the Bulwark Software Research.');
}

if (!isset($_POST['contact_name']) || !isset($_POST['contact_mobile']) || !isset($_POST['contact_email']) ||
    !isset($_POST['contact_organisation']) || !isset($_POST['contact_message'])) {
    Util::sendResponse(0, 'Partial data received from client side.');
}

include_once('connection.php');

$name = base64_decode($_POST['contact_name']);
$mobile = base64_decode($_POST['contact_mobile']);
$email = base64_decode($_POST['contact_email']);
$org = base64_decode($_POST['contact_organisation']);
$msg = base64_decode($_POST['contact_message']);

//2017-11-08 15:00:56
date_default_timezone_set('Asia/Kolkata');
$timestamp = date('Y-m-d H:i:s');
$user_type = "CLIENT";

$client_query = "INSERT INTO `query`
              (`qid`, `name`, `mobile`,`email`,`organisation_name`, `message`, `responded`, `timestamp`)
               VALUES(NULL, '$name', '$mobile', '$email','$org', '$msg','0','$timestamp');";

$query_rs = mysqli_query($connection, $client_query);

if (!$query_rs) {
    Util::sendResponse(0, "Failed to Query Server at this moment. Please try Again later.");
}

// soot mail to the concerned departments
mailingFunction($email, $name, $mobile, $msg);

Util::sendResponse(1, "Thank you:) We will contact you very soon.");


function mailingFunction($email_address, $name, $mobile, $message)
{


// Create the email for User and send them message
    $to_user = array($GLOBALS['email']);
    $to_name_user = array($GLOBALS['name']);
    $email_subject_user = "S.S. Kitchen Designer Contact Support";    // for user
    $email_body_user = "Hey $name,

			<br><br>Thank you for showing interest in  S.S. Kitchen Designer. We have received your query and will get back to you very soon.
			
			
			<br><br>Rajesh
			<br>S.S. Kitchen Designer & Refigeration (Sweet Bakers, Schools & Hotel kitchen Equipments).
            <br><a href='https://sskdesigner.com/'>sskdesigner.com</a>";

//send mail to Admin
    $email_subject_admin = "New Client Query";      // for admin
    $email_body_admin = "Hello Admin, A new user query is received via S.S. Kitchen Designer Website, Respond them soon...
						<br><br>Name: $name
						<br>Email: $email_address
						<br>Phone: $mobile
						<br><br>Message: $message<br>";

    $to_admin = array("info@sskdesigner.com","sskdesigner3@gmail.com");
    $to_name_admin = array("S.S. Kitchen Designer & Refigeration Admin","S.S. Kitchen Designer & Refigeration Admin" );


    sendMail($to_admin, $to_name_admin, $email_subject_admin, $email_body_admin);


    //send optional acknowledgement to the user
    sendMail($to_user, $to_name_user, $email_subject_user, $email_body_user);


}

?>
