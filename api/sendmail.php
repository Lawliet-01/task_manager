<?php

include_once("mailer.php");
/*error_reporting(E_ALL);
ini_set("display_errors",1);*/
// Check for empty fields
session_start();
if (empty($_POST['userid']) ||
        empty($_POST['email']) ||
        !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    echo json_encode(array("status" => 0, "message" => "No arguments Provided!"));
    exit;
}

$status = 1;
$msg = "Your message has been sent. ";

$name = strip_tags(htmlspecialchars($_POST['userid']));
$email_address = strip_tags(htmlspecialchars($_POST['email']));


// Create the email for User and send them message
$to_user = $email_address;
$to_name_user = $_SESSION["name"];
$newpass=$_SESSION["e_pass"];
$email_subject_user = "";    // for user
$email_body_user = "Hey $to_name_user,
      <br><br>Forgot Your Password.
  		<br><br>Here is Your New Pass Word: $newpass

      <br><br>WIDMITS.
      <br>";

//send mail to Admin
// $email_subject_admin = "New Client Query";      // for admin
// $email_body_admin = "Hello Admin, A new user query is received via S.S Kitchen designer, Respond them soon...
// 						<br><br>Name: $name
// 						<br>Email: $email_address";
//
// $to_admin = "widmits@gmail.com";
// $to_name_admin = "Ankit";

//testing email
/* $to_admin = array("mynameisakashkumar@gmail.com");
  $to_name_admin = array("BSR Admin");
  $email_subject_admin = "New Client Query"; */


sendMail($email_address, 'New Password', $email_body_user);
echo "success";

// if (sendMail($to_admin, $to_name_admin, $email_subject_admin, $email_body_admin)) {
//
//     //send optional acknowledgement to the user
//     sendMail($to_user, $to_name_user, $email_subject_user, $email_body_user);
//
//     header("Content-Type: application/json");
//     echo json_encode(array("status" => $status, "message" => $msg));
// } else {
//     header("Content-Type: application/json");
//     echo json_encode(array("status" => 0, "message" => "Message could not be sent. Please try again later."));
// }
exit;
?>
