<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../js/jquery.js"></script>
    <link rel="stylesheet" href="../css/materialize.css">
  </head>
  <body>
    <?php
    include "dbconn.php";
    $qry="SELECT * From `user_notes`";
    $rs=$conn->query($qry);

    if(mysqli_num_rows($rs)>0)
    {

      while($r=$rs->fetch_assoc())
      {
        ?>
        <div class="card-panel z-depth-5 blue">
        <?php
        echo $r['note'];
        ?>
      </div>
      <?php
      }

    }

     ?>

  </body>
</html>


<script type="text/javascript">
  $(document).ready(function(){
      CKEDITOR.replace( 'editor1' );
      $("#ccc").click(function(){
        var editorText = CKEDITOR.instances.edit.getData();
        console.log(editorText);
      });

  });


</script>
