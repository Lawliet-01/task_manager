<div class="card-panel blue accent-4 center white z-depth-3">
  <strong class="white-text">Change Password</strong>
</div>
<form class="card-panel White z-depth-5">
  <div class="input-field col s12 m12 l12">
    <i class="material-icons prefix">lock</i>
    <input id="oldpass" type="password">
    <label for="task_title">Old Password</label>
  </div>

  <div class="input-field col s12 m12 l12">
    <i class="material-icons prefix">lock</i>
    <input id="new1" type="password">
    <label for="task_title">New Password</label>
  </div>

  <div class="input-field col s12 m12 l12">
    <i class="material-icons prefix">lock</i>
    <input id="new2" type="password">
    <label for="new2">Re-enter New Password</label>
  </div>

  <div id="sub" class="input-field col s12 m12 l12 center">
    <button  class="btn blue accent-3" style="width:50vw;" type="button" name="button">Update</button>
  </div>
</form>
<script type="text/javascript">
  $(document).ready(function(){
    $('#sub').click(function(){
      var old=$('#oldpass').val();
      var new1=$('#new1').val();
      var new2=$('#new2').val();
      console.log(old+" "+new1+" "+new2);
      if(old!=""&&new1!=""&&new2!="")
      {
        if(new1==new2)
        {
          $.post("func_change_pass.php",{
            old:old,
            new:new1
          },function(data){
            console.log(data);
            if(data=="success")
            {
              Materialize.toast('Password Updated', 1500);

            }
            else if(data=="incorrect")
            {
              Materialize.toast('Old Password is Incorrect', 1500);

            }
            else
            {
              Materialize.toast('Someting Went Wrong', 1500);
            }

          });
        }
        else
        {
          Materialize.toast('Please Fill Same New Passwords', 1500);
        }
      }
      else
      {
        Materialize.toast('Please Fill all the Fields', 1500);

      }
    });

  });
</script>
