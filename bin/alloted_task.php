<div class="card-panel blue accent-4 center white z-depth-3">
  <a id="back" class="waves-effect waves-light white-text left"><i class="material-icons">reply</i></a>
  <strong class="white-text">Alloted Tasks</strong>
</div>
<div class="card-panel white z-depth-3  row">
  <div class="input-field col s10 m10 l10 push-s10">
    <!-- <i class="material-icons prefix">search</i> -->
    <input id="ttsrch" value="<?php if(isset($_SESSION['usertt'])){echo $_SESSION['usertt'];} ?>" list="tt_srch" placeholder="Task Title">
    <datalist id="tt_srch">
      <?php include('user_task_title_panel.php'); ?>
    </datalist>
  </div>
  <a id="srch" class="btn-floating  waves-effect waves-light blue"><i class="material-icons">search</i></a>
</div>

<div class="card-panel white z-depth-3" style="max-height:70vh; overflow-y:scroll;">
    <table  class="col l12 s6 m12 bordered">
      <thead id="tb_head">

      </thead>
      <tbody id="tb_content">

      </tbody>
    </table>

    <div id="full_comp" class="input-field col s12 m12 l12">
      <i class="material-icons prefix">date_range</i>
      <input id="comp_date" type="text" placeholder="Completion Date" onfocus="(this.type='date')" onblur="(this.type='text')">
    </div>
</div>

<div class="row">
    <div class="row col l3 s12 m12">
      <button id="sub" class="btn blue accent-3" type="button" name="button">Submit Sub Tasks</button>
    </div>
    <div class="row col l3 s12 m12">
      <button id="Comp_btn" class="btn blue accent-3" type="button" name="button">Submit Whole Task</button>
    </div>
    <div class="row col l3 s12 m12">
      <a id="addnote" href="#Notes"  class="btn blue accent-3">Add Notes</a>
    </div>
</div>

<div id="Notes" class="modal">
  <div class="modal-content">
    <div class="input-field">
        <textarea id="edit" name="editor1"></textarea>
    </div>
  </div>
  <div class="modal-footer">
    <a id="Ad" href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">ADD</a>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
      var r=[];
      var i=0;
      var comp_data={};
      var x=0;
      var task_title="";

      $("#back").hide();
      $("#Comp_btn").hide();
      $("#sub").hide();
      $("#full_comp").hide();
      $("#addnote").hide();

      if($("#ttsrch").val()==""){
        all_task();
      }
      else {
        task_title = $("#ttsrch").val();
        get_by_type(task_title);
      }


      $("#srch").click(function(){
          task_title = $("#ttsrch").val();
          if(task_title=="")
          {
            all_task();
          }
          else{

          //console.log(task_type+" "+task_title);
          get_by_type(task_title);
          }
      });

      $("#back").click(function(){
        $(this).hide();
        $("#full_comp").hide();
        $("#Comp_btn").hide();
        $("#addnote").hide();
        $("#sub").hide();
        get_by_type(task_title);
      });

      $("#Ad").click(function(){
        var editorText = CKEDITOR.instances.edit.getData();
        task_title = $("#ttsrch").val();
        if(editorText!=""&&task_title!="")
        {
          $.post("Func_add_notes.php",{
            note:editorText,
            tt:task_title
          },function(data){
            console.log(data);

          });
        }
      //  console.log(editorText);
      });

      $("#sub").click(function(){
        comp_data={};
        x=0;
        fill_array_cdate();
        var task_title = $("#ttsrch").val();
        console.log(comp_data);
        if(task_title!="")
        {
          $.post("Func_comp_sub_task.php",{
            task_title:task_title,
            comp_data:comp_data
          },function(data){
            console.log(data);
            if(data=="check")
            {
                Materialize.toast('Sub Task Updated Successfully', 1500);
            }
            else {
                Materialize.toast('Someting Went Wrong', 1500);
            }
          });
        }
        else {
            Materialize.toast('Select Proper Task Title', 1500);
        }

        //console.log(comp_data);
      });

      $("#Comp_btn").click(function(){
        var task_title=$("#ttsrch").val();
        var comp_date=$("#comp_date").val();
        if(task_title!=""){
          $.post("Func_comp_main_task.php",{
            task_title:task_title,
            comp_date:comp_date
          },function(data){
            console.log(data);
            if(data=="success")
            {
              Materialize.toast('Task Updated Successfully', 1500);
            }
            else if (data=="failed"){
              Materialize.toast('Sub Tasks are Pending', 1500);
            }
            else {
              Materialize.toast('Someting Went Wrong', 1500);
            }
          });
        }
        else{
          Materialize.toast('Fill Task Title', 1500);
        }
      });

      $("#addnote").click(function(){
        CKEDITOR.replace( 'editor1' );
        $('#Notes').modal();
        });

      function get_by_type(task_title){
        task_title=task_title;
        //alert(task_title);
        $.post("Func_get_task_for_user.php",{
          task_title:task_title
          },function(data){
            //console.log(data);
            fill_data_main(data);
          },"json");
      }

      function get_by_title(task_title){
        task_title=task_title;
        $.post("Func_get_task_by_title.php",{
          task_title:task_title
          },function(data){
            fill_data_sub(data);
          },"json");
      }

      function fill_data_sub(data){
          var title;
          i=0;
          $("#sub").removeClass('disabled');
          $("#tb_head").empty();
          $("#tb_content").empty();
          if(data!="")
          {
          $("#tb_head").append("<th>S.NO</th><th>Sub Task</th><th>Details</th><th>Status</th><th>Completion Date</th>");
          }
          $("#tb_content").empty();
          var sn=1;
          var date;
          $.each(data,function(key,value){
            var col=0;
            var chk=0;
            $("#tb_content").append("<tr>");
          $.each(value,function(colname,colvalue){

            if(col==0)
            {
                $("#tb_content").append("<td><span>"+sn+"</span></td>");
                sn++;
            }
            if(col==2)
            {
             title=colvalue;
             r[i]=title;
             i++;
            }
            if(col==4)
            {
              date=colvalue;
            }
            if(col>=2&&col!=4)
              {
              $("#tb_content").append("<td><span id='"+title+"sp"+"'>"+colvalue+"</span></td>");
              }

              //console.log(title);
            //  alert(task_title);

             if(col==5)
             {
               //alert(task_title);
              if(date=="0000-00-00")
               {
                $("#tb_content").append("<input class='cdate' id='"+title+"_date"+"' value='' type='text' placeholder='Completion Date' onfocus=(this.type='date') onblur=(this.type='text')>");
               }
               else
               {
                 $("#tb_content").append("<input class='cdate' id='"+title+"_date"+"' value='"+date+"' type='text' placeholder='Completion Date' fixed>");
               }

             }
              col++;
              //console.log(col);
              });

          $("#tb_content").append("</tr>");
          });

          $("#sub").show();
          $("#Comp_btn").show();
          $("#full_comp").show();
          $("#addnote").show();

          task_title = $("#ttsrch").val();

          $.post("func_get_comp_date.php",{task_title:task_title},function(data){
             console.log(data);
             cdate=data;
             console.log(cdate);
          if(cdate!="0000-00-00")
          {
            $("#full_comp").empty();
            $("#full_comp").append("<i class='material-icons prefix'>date_range</i>"+
                                   "<input class='cdate'  value='"+cdate+"' type='text' placeholder='Completion Date' fixed>"
                                  );
          }

          if(i==0)
          {
            $("#sub").addClass('disabled');
          }

         });
        }

      function fill_data_main(data){
          var title;
          $("#tb_head").empty();
          $("#tb_content").empty();
          $("#tb_head").append("<th>Task Title</th><th>Type</th><th>Details</th><th>Assign To</th><th>Assign Date</th><th>Estimated Date</th><th>Follow Up</th><th>Status</th><th>View</th>");
          $("#tb_content").empty();

          $.each(data,function(key,value){
            var col=0;
            var chk=0;
            $("#tb_content").append("<tr>");
          $.each(value,function(colname,colvalue){

            if(col==1)
            {
             title=colvalue;
            }
            if(col!=9&&col!=0)
            {
              $("#tb_content").append("<td><span   id='"+title+"sp"+"'>"+colvalue+"</span></td>");
            }
              //console.log(title);
            if(col==9){
              $("#tb_content").append("<td><a id='"+title+"' class='viewbut btn-floating  waves-effect waves-light blue'><i class='material-icons'>view_list</i></a></span></td>");
            }
              col++;
              //console.log(col);
              });
          $("#tb_content").append("</tr>");
          });
          $("#sub").hide();
          $("#Comp_btn").hide();
          $("#full_comp").hide();
          $("#addnote").hide();

            viewclick();
        }

      function viewclick(){
        $(".viewbut").click(function(){
          var title=$(this).attr('id');
          task_title=title;
          $("#ttsrch").val(title);
          $("#back").show();
          $("#full_comp").show();
          $("#Comp_btn").show();
          $("#addnote").show();
          $("#sub").show();
          get_by_title(task_title);
          });
      }

      function fill_array_cdate(){
        $.each(r,function(key,value){
        var row=[];
        console.log(value);
        row[0]=value;
        var subid=value+"_date";
        row[1]=$("input[id='"+subid+"']").val();
        comp_data[x]=row;
        x=x+1;
        });
        }

      function all_task(){
        $.post("Func_get_alltask_for_user.php",{

          },function(data){
            //console.log(data);
            fill_data_main(data);
          },"json");
      }


    });
  </script>
