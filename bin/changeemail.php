<div class="card-panel blue accent-4 center white z-depth-3">
  <strong class="white-text">Change Email</strong>
</div>
<form class="card-panel White z-depth-5">
  <div class="input-field col s12 m12 l12">
    <i class="material-icons prefix">mail</i>
    <input id="email" type="text">
    <label for="email">New Email</label>
  </div>

  <div id="sub" class="input-field col s12 m12 l12 center">
    <button  class="btn blue accent-3" style="width:50vw;" type="button" name="button">Update</button>
  </div>
</form>
<script type="text/javascript">
  $(document).ready(function(){
    $('#sub').click(function(){
      var email=$('#email').val();

      console.log(email)
      if(email!="")
      {
        if(validateEmail(email))
        {
          $.post("func_change_email.php",{
            email:email
          },function(data){
            console.log(data);
            if(data=="success")
            {
              Materialize.toast('Email Updated', 1500);

            }
            else
            {
              Materialize.toast('Someting Went Wrong', 1500);
            }

          });
        }
        else
        {
          Materialize.toast('Please Enter a valid Email', 1500);
        }
      }
      else
      {
        Materialize.toast('Please Enter an Email', 1500);

      }
    });


    //-----------------------------------------------------
    function validateEmail(sEmail)
    {
      var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
      if (filter.test(sEmail)){
        return true;
      }
      else
      {
          return false;
      }
    }

  });
</script>
