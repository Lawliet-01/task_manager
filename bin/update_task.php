<div class="card-panel blue accent-4 center white z-depth-3">
  <a id="back" class="waves-effect waves-light white-text left"><i class="material-icons">reply</i></a>
  <strong class="white-text">UPDATE TASK</strong>
</div>

<div class="card-panel white z-depth-3  row center">
  <div class="input-field col s4 m4 l4">
    <!-- <i class="material-icons prefix">search</i> -->
    <input id="tsrch" list="t_srch" placeholder="Task Type">
    <datalist id="t_srch">
      <?php include('task_type_panel.php'); ?>
    </datalist>
  </div>

  <div class="input-field col s4 m4 l4">
    <!-- <i class="material-icons prefix">search</i> -->
    <input id="ttsrch" list="tt_srch" placeholder="Task Title">
    <datalist id="tt_srch">
      <?php include('task_title_panel.php'); ?>
    </datalist>
  </div>

  <a id="srch" class="btn-floating  waves-effect waves-light blue"><i class="material-icons">search</i></a>
</div>
<div class="card-panel white z-depth-3" style="max-height:70vh; overflow-y:scroll;">
  <table class="col l12 s6 m12 bordered">
    <thead id="tb_head">

    </thead>
    <tbody id="tb_content">
    <tbody>
      <div id="box" class="">

      </div>

    </tbody>
  </table>
  <div class="">
    <div id="full_comp" class="input-field col s12 m12 l12">
      <i class="material-icons prefix">date_range</i>
      <input id="comp_date" type="text" placeholder="Completion Date" value='' onfocus="(this.type='date')" onblur="(this.type='text')">
    </div>
    <div id="flw" class="input-field col s12 m12 l12">
      <i class="material-icons prefix">date_range</i>
      <label for="fl_date">Change Follow Up</label>
      <input id="fl_date" type="text" placeholder="Chnage Follow Up" value="" onfocus="(this.type='date')" onblur="(this.type='text')">
    </div>
    <div class="input-field col s12 m12 l12">
      <button id="viewnotes" class="btn  blue accent-3 waves-effect waves-light" type="button" name="button">View Notes</button>
    </div>
  </div>

</div>

<div class="row">
  <div class="row col l4 s12 m12">
    <button id="sub" class="btn blue accent-3" type="button" name="button">Submit Sub Tasks</button>
  </div>
  <div class="row col l4 s12 m12">
    <button id="Chng_flw" class="btn blue accent-3" type="button" name="button">Change Follow Up</button>
  </div>
  <div class="row col l4 s12 m12">
    <button id="Comp_btn" class="btn blue accent-3" type="button" name="button">Submit Whole Task</button>
  </div>

</div>

<script type="text/javascript">
  $(document).ready(function() {
    var r = [];
    var i = 0;
    var comp_data = {};
    var x = 0;
    var task_type = "";
    var task_title = "";
    var myses = "";
    var cdate;


    $("#Chng_flw").hide();
    $("#flw").hide();
    $("#back").hide();
    $("#Comp_btn").hide();
    $("#sub").hide();
    $("#full_comp").hide();
    $("#viewnotes").hide();


    $("#srch").click(function() {
      task_type = $("#tsrch").val();
      task_title = $("#ttsrch").val();

      //console.log(task_type+" "+task_title);
      if (task_title == "") {
        get_by_type(task_type);
      } else {
        get_by_title(task_title);
      }
    });

    $("#back").click(function() {
      $(this).hide();
      $("#full_comp").hide();
      $("#Comp_btn").hide();
      $("#sub").hide();
      $("#flw").hide();
      $("#Chng_flw").hide();
      $("#viewnotes").hide();

      get_by_type(task_type);
    });

    $("#sub").click(function() {
      comp_data = {};
      x = 0;
      fill_array_cdate();
      var task_title = $("#ttsrch").val();
      if (task_title != "") {
        $.post("Func_comp_sub_task.php", {
          task_title: task_title,
          comp_data: comp_data
        }, function(data) {
          console.log(data);
          if (data == "check") {
            Materialize.toast('Sub Task Updated Successfully', 1500);
          } else {
            Materialize.toast('Someting Went Wrong', 1500);
          }
        });
      } else {
        Materialize.toast('Select Proper Task Title', 1500);
      }

      //console.log(comp_data);
    });

    $("#Chng_flw").click(function() {
      var flw = $("#fl_date").val();
      $.post("Func_change_fllw.php", {
        flw: flw,
        tt: task_title
      }, function(data) {
        console.log(data);
        if (data == "success") {
          Materialize.toast('Follow Up Changed', 1500);
        } else {
          Materialize.toast('Someting Went Wrong', 1500);
        }

      });
    });

    $("#Comp_btn").click(function() {
      var task_title = $("#ttsrch").val();
      var comp_date = $("#comp_date").val();
      if (task_title != "") {
        $.post("Func_comp_main_task.php", {
          task_title: task_title,
          comp_date: comp_date
        }, function(data) {
          console.log(data);
          if (data == "success") {
            Materialize.toast('Task Updated Successfully', 1500);
          } else if (data == "failed") {
            Materialize.toast('Sub Tasks are Pending', 1500);
          } else {
            Materialize.toast('Someting Went Wrong', 1500);
          }
        });
      } else {
        Materialize.toast('Fill Task Title', 1500);
      }
    });

    $("#viewnotes").click(function() {
      task_title = $("#ttsrch").val();
      //  alert("click");
      $.post("Func_get_notes.php", {
        tt: task_title
      }, function(data) {
        console.log(data);

        if (data[0] == "") {
          Materialize.toast('No Notes Found', 1500);
        } else {
          fill_notes(data);
        }
      }, "json");
    });

    // GET BY TITLE
    function get_by_title(task_title) {

      $.post("Func_get_task_by_title.php", {
        task_title: task_title
      }, function(data) {
        console.log(data);

        fill_data_title(data);
      }, "json");
    }

    function get_by_type(task_type) {
      $.post("Func_get_task_by_type.php", {
        task_type: task_type
      }, function(data) {
        fill_data_type(data);
      }, "json");
    }

    function fill_data_title(data) {
      var title;
      i = 0;
      $("#sub").removeClass('disabled');
      $("#box").empty();
      $("#tb_content").empty();
      $("#tb_head").empty();
      if (data != "") {
        $("#tb_head").append("<th>S.NO</th><th>Sub Task</th><th>Details</th><th>Status</th><th>Completion Date</th><th>Changes</th>");
      }
      $("#tb_content").empty();
      var sn = 1;
      var date;
      $.each(data, function(key, value) {
        var col = 0;
        var chk = 0;
        $("#tb_content").append("<tr>");
        $.each(value, function(colname, colvalue) {

          if (col == 0) {
            $("#tb_content").append("<td><span>" + sn + "</span></td>");
            sn++;
          }
          if (col == 2) {
            title = colvalue;
            r[i] = title;
            i++;
          }
          if (col == 4) {
            date = colvalue;
          }
          if (col >= 2 && col != 4) {
            $("#tb_content").append("<td><span id='" + title + "sp" + "'>" + colvalue + "</span></td>");
          }
          //console.log(title);
          if (col == 5) {
            if (date == "0000-00-00") {
              $("#tb_content").append("<input class='cdate' id='" + title + "_date" + "' value='' type='text' placeholder='Completion Date' onfocus=(this.type='date') onblur=(this.type='text')>");
            } else {
              $("#tb_content").append("<input class='cdate' id='" + title + "_date" + "' value='" + date + "' type='text' placeholder='Completion Date' fixed>");
            }
            col++;
          }
          if (col == 6) {
            $("#tb_content").append("<td><a id='" + title + "' class='reversebtn btn-floating  waves-effect waves-light blue'><i class='material-icons'>reply</i></a></td>");
          }
          col++;
          //console.log(col);
        });

        $("#tb_content").append("</tr>");
      });

      $("#sub").show();
      $("#Comp_btn").show();
      $("#full_comp").show();
      $("#viewnotes").show();

      $.post("func_get_comp_date.php", {
        task_title: task_title
      }, function(data) {
        console.log(data);
        cdate = data;
        console.log(cdate);
        if (cdate != "0000-00-00") {
          var tt = $("#ttsrch").val();
          $("#full_comp").empty();
          $("#full_comp").append("<i class='material-icons prefix'>date_range</i>" +
            "<input id='" + tt + "_date' class='cdate'  value='" + cdate + "' type='text' placeholder='Completion Date' fixed>" +
            "<a id='" + tt + "' class='rev btn  waves-effect waves-light blue'><i class='material-icons left'>reply</i>Revert</a>"
          );
        }

        reversewhole();
      });
      reverseclick();

      $("#flw").show();
      $("#Chng_flw").show();
      $("#viewnotes").show();

      $.post("func_get_follow_date.php", {
        tt: task_title
      }, function(data) {
        console.log(data);
        $("#fl_date").val(data);
        $("#fl_date").trigger('focus');
      }, "json");

      if (i == 0) {
        $("#sub").addClass('disabled');
      }
    }

    function fill_data_type(data) {
      var title;

      $("#flw").hide();
      $("#box").empty();
      $("#tb_content").empty();
      if (data[0] == "") {
        Materialize.toast('No Task Found', 1500);
      }
      $("#tb_head").empty();
      $("#tb_content").empty();
      $("#tb_head").append("<th>Task Title</th><th>Type</th><th>Details</th><th>Assign To</th><th>Assign Date</th><th>Estimated Date</th><th>Follow Up</th><th>Status</th><th>View</th>");
      $("#tb_content").empty();

      $.each(data, function(key, value) {
        var col = 0;
        var chk = 0;
        $("#tb_content").append("<tr>");
        $.each(value, function(colname, colvalue) {

          if (col == 1) {
            title = colvalue;
          }
          if (col == 7) {
            flwdate = colvalue;
          }
          if (col != 9 && col != 0) {
            $("#tb_content").append("<td><span   id='" + title + "sp" + "'>" + colvalue + "</span></td>");
          }
          //console.log(title);
          if (col == 9) {
            $("#tb_content").append("<td><a id='" + title + "' class='viewbut btn-floating  waves-effect waves-light blue'><i class='material-icons'>view_list</i></a></span></td>");
          }
          col++;
          //console.log(col);
        });
        $("#tb_content").append("</tr>");
      });
      $("#sub").hide();
      $("#Comp_btn").hide();
      $("#full_comp").hide();
      $("#Chng_flw").hide();
      $("#viewnotes").hide();
      viewclick();
    }

    function viewclick() {
      $(".viewbut").click(function() {
        var title = $(this).attr('id');
        task_title = title;
        $("#ttsrch").val(title);
        $("#back").show();
        $("#full_comp").show();
        $("#Comp_btn").show();
        $("#viewnotes").show();
        $("#sub").show();
        console.log(task_title);
        get_by_title(task_title);
      });
    }

    function reverseclick() {
      $(".reversebtn").click(function() {
        var title = $(this).attr('id');
        sub_tt = title;
        myid = title + "_date";
        var cdate = $("input[id='" + myid + "']").val();
        if (cdate != "") {
          if (confirm("Do you want to make changes in task??")) {
            var tt = $("#ttsrch").val();
            $.post("func_reverse_task.php", {
              sub_tt: sub_tt,
              tt: tt
            }, function(data) {
              get_by_title(task_title);
              //console.log(data);
            });
          } else {
            console.log("no");
          }
        }
      });
    }

    function reversewhole() {
      $(".rev").click(function() {
        if (confirm("Do you want to make changes in task??")) {
          var task_title = $(this).attr('id');

          $.post("func_reverse_main_task.php", {
            tt: task_title
          }, function(data) {
            console.log(data);
            get_by_title(task_title);
            //console.log(data);
          });
        } else {
          console.log("no");
        }

      });
    }

    function fill_array_cdate() {
      $.each(r, function(key, value) {
        var row = [];
        console.log(value);
        row[0] = value;
        var subid = value + "_date";
        row[1] = $("input[id='" + subid + "']").val();
        comp_data[x] = row;
        x = x + 1;
      });

    }

    function fill_notes(data) {
      $("#flw").hide();
      $("#tb_content").empty();
      $("#tb_head").empty();
      // $("#tb_content").append("<ul class='collapsible' data-collapsible='accordion'>");

      var count = 1;
      $.each(data, function(key, value) {
        // $("#tb_content").append("<tr>");
        //console.log(value["id"]+" "+value["task_title"]+" "+value["note"]);
        $("#box").append("<ul class='collapsible' data-collapsible='accordion'><li><div class='collapsible-header'>Note : " + count + "</div>" +
          "<div class='collapsible-body'>" + value['note'] + "<br>Date : " + value['date'] + "</div>" +
          "</li></ul>");
        count++;

      });
      //  $("#box").append("");
      $("#sub").hide();
      $("#Comp_btn").hide();
      $("#full_comp").hide();
      $("#Chng_flw").hide();
      $("#viewnotes").hide();
      $('.collapsible').collapsible();
    }
    $('.collapsible').collapsible();
  });
</script>