<div class="card-panel blue accent-4 center white z-depth-3">
  <strong class="white-text">ASSIGN TASK</strong>
</div>
<form class="card-panel  white z-depth-3">

  <div class="input-field col s12 m12 l12">
    <i class="material-icons prefix">title</i>
    <input id="task_title" type="text">
    <label for="task_title">Task Title</label>
  </div>

  <div class="input-field col s12 m12 l12">
    <i class="material-icons prefix">assignment</i>
    <input id="tlist" list="task_type" placeholder="Task Type">
    <datalist id="task_type">
      <?php include('task_type_panel.php'); ?>
    </datalist>
  </div>

  <div class="input-field col s12 m12 l12">
    <i class="material-icons prefix">assignment_ind</i>
    <input id="talist" list="task_assign" placeholder="Assigned To">
    <datalist id="task_assign">
      <?php include('task_assign_panel.php'); ?>
    </datalist>
  </div>

  <div class="input-field col s12">
    <i class="material-icons prefix">create</i>
    <textarea id="task_details" class="materialize-textarea"></textarea>
    <label for="task_details">Task Details</label>
  </div>

  <div class="input-field col s12 m12 l12">
    <i class="material-icons prefix">date_range</i>
    <label for="a_date">Assigned Date</label>
    <input id="a_date" type="text" value="<?php echo date("Y-m-d");?>" placeholder="Assigned Date" onfocus="(this.type='date')" onblur="(this.type='text')">
  </div>

  <div class="input-field col s12 m12 l12">
    <i class="material-icons prefix">date_range</i>
    <input id="sub_date" type="text" placeholder="Estimated Submission Date" onfocus="(this.type='date')" onblur="(this.type='text')">
  </div>

  <div class="input-field col s12 m12 l12">
    <i class="material-icons prefix">date_range</i>
    <input id="fllw_date" type="text" placeholder="Follow-Up Date" onfocus="(this.type='date')" onblur="(this.type='text')">
  </div>

  <div id="assign_btn" class="input-field col s12 m12 l12 center">
    <button  class="btn blue accent-3" style="width:50vw;" type="button" name="button">Assign</button>
  </div>

</form>
<script type="text/javascript">
$(document).ready(function(){

    $("#a_date").trigger('focus');
    $("#assign_btn").click(function(){
    //console.log("hello");
      ttitle=$("#task_title").val();
      tlist=$("#tlist").val();
      talist=$("#talist").val();
      a_date=$("#a_date").val();
      fllw_up= $("#fllw_date").val();
      task_details=$("#task_details").val();
      sub_date=$("#sub_date").val();
      //console.log(a_date+" "+sub_date);


      if(tlist!=""&&talist!=""&&a_date!=""&&sub_date!=""&&ttitle!="")
      {
          $.post("Func_assign_task.php",{
            ttitle:ttitle,
            task:tlist,
            assign:talist,
            a_date:a_date,
            sub_date:sub_date,
            task_details:task_details,
            fllw_up:fllw_up
          },function(data){
            console.log(data);
              if(data=="success"){
                //console.log(Cookies.get("title"));
                $("#maincontainer").load("sub_task.php");
              }
              else if(data=="failed") {
                Materialize.toast('Task Already Exist', 1500);
              }
              else if(data=="no_employee")
              {
                  Materialize.toast('Enter Valid User In \"Assigned To\"', 1500);
              }
              else
              {
                 Materialize.toast('Something went wrong', 1500);
              }
          });
        }
        else
        {
          Materialize.toast('Fill All Details', 1500);
        }
  });

});

</script>
