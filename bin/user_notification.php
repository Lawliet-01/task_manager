<div class="card-panel blue accent-4 center white z-depth-3">
  <strong class="white-text">NOTIFICATIONS</strong>
</div>
<div class="row">

  <ul class="collapsible popout" data-collapsible="accordion">
    <?php
      session_start();
      include "dbconn.php";

      $name=$_SESSION['name'];
      $qry="SELECT * FROM `user_notification` Where `assigned_to`='$name' And `status`='Pending'";
      $rs=$conn->query($qry);
      if(mysqli_num_rows($rs)>0)
      {
        while($row=$rs->fetch_assoc())
        {
     ?>

    <li id="<?php echo $row['id']; ?>" data-ttitle="<?php echo $row['task_title']; ?>">
      <div  class="collapsible-header "><?php echo $row['not_type']; ?></div>
      <div class="collapsible-body ">
        <div><strong>Task Title : </strong><?php echo $row['task_title']; ?></div><br>
        <div><strong>Task Details : </strong><?php echo $row['task_details']; ?></div><br>
        <div class="col l3 m3 s12">
          <button  id="<?php echo $row['task_title'].'btn'; ?>" class="viewbtn btn waves-effect waves-light blue" type="button" name="button">View Task</button>
        </div>
      </div>
    </li>

   <?php
        }
      }
      else {
        echo '<div><strong>No New Notifications </strong></div><br>';
      }
    ?>
  </ul>
</div>

<script type="text/javascript">
  $(document).ready(function(){
     $('.collapsible').collapsible({
       onOpen: function(el)
       {
         var tt=el[0].attributes[1].nodeValue;
         var nid=el[0].id;
        $.post("func_user_not_update.php",{tt:tt,nid:nid},function(data){
           console.log(data);
        });
        viewclick(el);
         // console.log(el[0].attributes[1].nodeValue);
         // console.log(el[0].id);
       } // Callback for Collapsible open
     });

     function viewclick(el){
       $(".viewbtn").click(function(){
            $("#maincontainer").load("alloted_task.php");
          //get_by_title(task_title);
         });
       }
  });
</script>
