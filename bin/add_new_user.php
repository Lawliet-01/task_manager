<div class="card-panel blue accent-4 center white z-depth-3">
  <strong class="white-text">ADD EMPLOYEES</strong>
</div>
<form class="card-panel  white z-depth-3">
  <div class="input-field col s12 m12 l12">
    <i class="material-icons prefix">account_circle</i>
    <input  id="e_id" type="text">
    <label for="e_id">Employee Id</label>
  </div>

  <div class="input-field col s12 m12 l12">
    <i class="material-icons prefix">face</i>
    <input  id="name" type="text">
    <label for="name">Name</label>
  </div>

  <div class="input-field col s12 m12 l12">
    <i class="material-icons prefix">lock</i>
    <input  id="epass" type="password">
    <label for="epass">Password</label>
  </div>

  <div  id="add_user" class="input-field col l6 m6 s6 center">
    <a class="btn blue accent-2" href="#" style="width:80%">Add User</a>
  </div>
</form>
<script type="text/javascript">
  $(document).ready(function(){

    $("#add_user").click(function(){  //Funtion to be performed when user click btn
      //--------------------All variables--------------------
        var e_id=$("#e_id").val();
        var name=$("#name").val();
        var pass=$("#epass").val();
        if(pass=="")
        {
          pass="default";
        }
      //------------------------------------------------------
      if(e_id!=""&&pass!="")
      {

        $.post("Func_add_employee.php",{
                e_id:e_id,
                pass:pass,
                name:name
              },function(data){

                console.log(data);
                if(data=="success")
                {
                  Materialize.toast('Employee Added Successfully', 1500);
                }
                else if(data=="failed")
                {
                  Materialize.toast('Name Or Employee-ID Already Exist', 1500);
                }
               else
                {
                  Materialize.toast('Something Went Wrong', 1500);
                }
            });
        }
        else
        {
          Materialize.toast('Enter All Details', 1500);
        }
     });




  });
</script>
