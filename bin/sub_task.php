<div class="card-panel blue accent-4 center white z-depth-3">
  <strong class="white-text">Sub Task</strong>
</div>
<?php session_start(); ?>
<div class="card-panel white z-depth-3 input-field col s6 m6 l6">
    <i class="material-icons prefix">search</i>
    <input id="tsearch" list="task_search" value="<?php if(isset($_SESSION["ttitle"])){echo $_SESSION["ttitle"]; } ?>" placeholder="Task Type">
    <datalist id="task_search">
      <?php include('task_title_panel.php'); ?>
    </datalist>
</div>
<div class="card-panel white z-depth-3" style="max-height:80vh; overflow-y:scroll">
    <div class="row">
        <a id="add" class="btn-floating  waves-effect waves-light blue"><i class="material-icons">add</i></a>
        <span style="padding:1vw;"><strong>Add Sub Task</strong></span>
    </div>
    <div class="divider"></div>

    <table  class="class col l12 s6 m12" style="max-height:200px; overflow-y:scroll;">
      <tbody id="t_content">

      </tbody>
    </table>
</div>
<div class="col l12 s12 m12 center">
  <button id="sub_assign" class="btn blue" type="button" name="button">Assign Sub-Tasks</button>
</div>


<script type="text/javascript">
  $(document).ready(function(){
    var i=1;
    var sub_data={};
    $("#add").click(function(){

      $("#t_content").append("<tr><td>"+
          "<div class='input-field col s12 m12 l12'>"+
          "<i class='material-icons prefix'>title</i>"+
          "<input id='task_title_"+i+"' type='text'>"+
          "<label for='task_title_"+i+"'>Task Title "+i+"</label>"+
          "</div>"+

          "<div class='input-field col s12'>"+
          "<i class='material-icons prefix'>create</i>"+
          "<textarea id='task_details_"+i+"' class='materialize-textarea' ></textarea>"+
          "<label for='task_details_"+i+"'>Sub_Task Details "+i+"</label>"+
        "</div>"+
      "<div class='divider'></div></td></tr>");
      i=i+1;
    });

    $("#sub_assign").click(function(){
      for(var x=1;x<i;x++){
      //  console.log($("#task_details_"+x).val());
        var row=[];
        row[0]=$("#task_title_"+x).val();
        row[1]=$("#task_details_"+x).val();
        sub_data[x-1]=row;
        var ttitle=$("#tsearch").val();
      }
      //console.log(sub_data);
      $.post("Func_assign_sub_task.php",{
        sub_data:sub_data,
        ttitle:ttitle

        },function(data){
        //  console.log(data);
          if(data!="")
          {
              Materialize.toast('Sub Tasks Assigned Successfully', 1500);
                $("#t_content").empty();
          }
          else{
            Materialize.toast('Fill Atleast One Sub Task', 1500);
          }
      });
    });

  });
</script>
