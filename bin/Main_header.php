<?php
include 'Func_validate.php';
  if(!validate())
 {
 header("Location: ../index.php");
 }
?>
<html>
<head>
<title>TASK MANAGER</title>
    <!--Import Google Icon Font-->
    <!-- <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
     <link  rel="stylesheet" href="../fonts/icons/icon.woff2" >
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" href="../css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Importing javascript files -->
    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/jquery.cookiejs.js"></script>
    <script type="text/javascript" src="../js/materialize.min.js"></script>
    <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
</head>

<body class="grey lighten-3">
<header >
  <nav class="blue darken-2" style="">
      <ul>
        <li>
          <a href="#" id="sidenavshow" data-activates="slide-out" class="btn-floating  blue darken-1 waves-effect waves-light"> <i class="material-icons left">menu</i></a>
        </li>
        <li><span class="brand-logo center">Task Manager</span></li>
      </ul>
  </nav>
</header>

<div class="row" >
    <ul id="slide-out" class="side-nav">
      <?php include("cpanel.php");?>
      <li><a id="logout" class="waves-effect "  href="#!">LOGOUT</a></li>
      <li><div class="divider"></div></li>
    </ul>
</div>

<div class="container" style="height:100%;">
  <div id="maincontainer" class="col l12">
        This is main container
  </div>
</div>

</body>

</html>
<script type="text/javascript">
$(document).ready(function(){


  // Initialize collapse button
  $(".btn-floating").sideNav();

  $('.btn-floating').sideNav({
      menuWidth: 300, // Default is 300
      closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      draggable: true, // Choose whether you can drag to open on touch screens,
    });
    //console.log($.session.get('type'));


    var type=<?php echo json_encode($_SESSION['type']); ?>;
    if(type=="Admin")
    {
    $("#maincontainer").load("assign_task.php");
      onreload();
    }
    else {
      $("#maincontainer").load("alloted_task.php");
      onreload();
    }
      $('#logout').click(function(){ //Function to be performed when logout is clicked
        var x=$.cookie("userid");
        var y=$.cookie("PHPSESSID");
        $.post("Func_logout.php",{
             userid:x,
             sessid:y
             },function(data){
             console.log(data);
             if(data=="success")
             {
               Materialize.toast('Logout Successfully', 1500);
               window.open("../index.php","_self");
             }
             else
             {
               Materialize.toast('Someting Went Wrong', 1500);
             }
           });
      });



    $(".menubar").click(function(){ //Function to load pages inside the maincontainer div
      var page = $(this).data("value")+".php";
      ///alert(page);
        localStorage.setItem("page",page);
        $("#maincontainer").load($(this).data("value")+".php");
        $("#sidenav-overlay").trigger("click");
    });

    function onreload(){
      if(performance.navigation.type == performance.navigation.TYPE_RELOAD)
      {
        var cp=localStorage.getItem("page");
        $("#maincontainer").load(cp);
        $("#sidenav-overlay").trigger("click");
      }
    }

  });
</script>
