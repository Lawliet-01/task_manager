<div class="card-panel blue accent-4 center white z-depth-3">
  <strong class="white-text">TASK REPORT</strong>
</div>
<div class="row card-panel white z-depth-3 center">
    <div class="input-field col s4 m4 l4">
      <i class="material-icons prefix">date_range</i>
      <label for="s_date">From</label>
      <input id="s_date" type="text" value=""  onfocus="(this.type='date')" onblur="(this.type='text')">
    </div>
    <div class="input-field col s4 m4 l4">
      <i class="material-icons prefix">date_range</i>
      <label for="e_date">To</label>
      <input id="e_date" type="text" value="" onfocus="(this.type='date')" onblur="(this.type='text')">
    </div>
    <div class="col s4 m4 l4">
      <a id="srch" class="btn-floating  waves-effect waves-light blue"><i class="material-icons">search</i></a>
    </div>
</div>

<div class="card-panel white z-depth-3" style="max-height:70vh; overflow-y:scroll;">
  <table  class="col l12 s6 m12 bordered">
    <thead id="tsk_head">

    </thead>
    <tbody id="tsk_content">

    </tbody>
  </table>
</div>
<script type="text/javascript">
  $(document).ready(function(){

    all_task();
    $("#srch").click(function(){
      var sd = $("#s_date").val();
      var ed = $("#e_date").val();
      console.log(sd+" "+ed);
      if(sd!="")
      {
        $.post("Func_get_task_by_range.php",{
          sd:sd,
          ed:ed
        },function(data){
          console.log(data);
          fill_data(data);
        },"json");
      }
      else{
        all_task();
      }
    });

    function all_task()
    {
      $.post("Func_get_alltask.php",{
        },function(data){
          console.log(data);
          fill_data(data);
        },"json");
    }

    function fill_data(data){
        var title;
        $("#tsk_head").empty();
        $("#tsk_head").append("<th>Task Title</th><th>Type</th><th>Details</th><th>Assign To</th><th>Assign Date</th><th>Estimated Date</th><th>Follow Up</th><th>Status</th><th>Completed On</th>");
        $("#tsk_content").empty();

        $.each(data,function(key,value){
          var col=0;
          var chk=0;
          $("#tsk_content").append("<tr>");
        $.each(value,function(colname,colvalue){

          if(col==1)
          {
           title=colvalue;
          }
          if(col!=0)
          {
            $("#tsk_content").append("<td><span   id='"+title+"sp"+"'>"+colvalue+"</span></td>");
          }
            //console.log(title);
          // if(col==7){
          //   $("#tb_content").append("<td><a id='"+title+"' class='viewbut btn-floating  waves-effect waves-light blue'><i class='material-icons'>view_list</i></a></span></td>");
          // }
            col++;
            //console.log(col);
            });
        $("#tsk_content").append("</tr>");
        });

      }

  });
</script>
