<?php
include 'bin/Func_validate.php';
 if(validate())
 {
 header("Location: bin/Main_header.php");
 }
 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Task_Manager</title>
    <!--Import Google Icon Font-->
    <!-- <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
     <link  rel="stylesheet" href="fonts/icons/icon.woff2" >
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link href="css/slider.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css">
    <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Importing javascript files -->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/cookiejs.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
   <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.3.0.js"></script> -->
    <script type="text/javascript" src="js/jquery_Migrate.js"></script>
  </head>
  <body class="white">

    <div class="container row" style="margin-top:2%;">
      <div class="col s12 m6 l6 offset-m3 offset-l3 white z-depth-4">
        <!-- LOGIN PAGE STARTS HERE -->
          <div id="log_tab" class="row">
            <div class="input-field center col s12 m12 l12">

            <form class="login-form">
                <div class=" col s12">
                    <img class="responsive-img" src="image/logoiimt.png" alt="LOGO">
                  </div>

                  <div class="input-field col s12 m12 l12">
                    <i class="material-icons prefix">account_circle</i>
                    <input  id="user_id" type="text">
                    <label for="user_id">Userid</label>
                  </div>

                  <div class="input-field col s12 m12 l12">
                  <i class="material-icons prefix">lock</i>
                  <input  id="password" type="password">
                  <label for="password">Password</label>
                  </div>

                  <div  class="input-field col s12">
                    <a  id="login_btn" class="btn blue accent-2" href="#" style="width:100%;">LOGIN</a>
                  </div>

                  <div class="col s12 center">
                      <a id="forget" class="waves-effect waves-light modal-trigger" href="#modal1"><span class="red-text">Forget Password</span></a>
                  </div>
              </form>
        </div>
      </div>
    </div>
        <!-- LOGIN PAGE ENDS HERE -->

      </div>

    </div>

  <!-- Modal Structure -->

  <div id="info" class="modal">
    <div class="modal-content">
      <div class="input-field">
        Please Check Email Associated with your account for the new Password
      </div>
    </div>
    <div class="modal-footer">
      <a  href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
    </div>
  </div>

  <div id="modal1" class="modal">
    <div class="modal-content">
      <div class="input-field">
        <input id="u_id" type="text" name="" value="">
        <label for="u_id">Enter Your User-id</label>
      </div>
    </div>
    <div class="modal-footer">
      <a id="ok" href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
    </div>
  </div>




  </body>

<script type="text/javascript">
  $(document).ready(function(){

    $("#login_btn").click(function(){  //Funtion to be performed when user click login
      //--------------------All variables--------------------
        var userid=$("#user_id").val();
        var pass=$("#password").val();
      //------------------------------------------------------
      if(user_id!=""&&pass!="")
      {
      $.post("bin/Func_login.php",{
              userid:userid,
              pass:pass
            },function(data){
          //  alert(data);
              console.log(data);
              if(data=="success")
              {
                Materialize.toast('Logined Successfully', 1500);
                window.open("bin/Main_header.php#","_self");
              }
              else if(data="error")
              {
                Materialize.toast('Enter Valid User-id and Password', 1500);
              }
          });
      }
      else
      {
        Materialize.toast('Enter All Details', 1500);
      }
     });


     $("#forget").click(function(){
        $('#modal1').modal();
      });

     $("#ok").click(function(){
       var userid=$("#u_id").val();
         $('#modal1').modal('close');
         if(userid!="")
         {
            $.post("bin/Func_check_user.php",{userid:userid},function(data)
            {

              if(data=="Nomail")
                {
                Materialize.toast('No Email is Associated with your account', 2000);
                }
                if(data!="Nomail" && data!="failed"){
                  Materialize.toast('Please Check Email Associated With your Account', 2000);

                  $.post("api/sendmail.php",{
                    email:data,
                    userid:userid
                  },function(data){
                    console.log(data);
                  });
              }
              if(data=="failed")
              {
                Materialize.toast('Invalid Account', 1500);

              }
            });
         }
         else
         {
           Materialize.toast('Enter User Id', 1500);
         }
     });

      //<<<<<<<<<<<<<<<-------OTHER FUNCTIONS For Registeration process for later use------------->>>>>>>>>>>>>>>>>>

      //Email validate
      // function validateEmail(sEmail)
      // {
      //   var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
      //   if (filter.test(sEmail)){
      //     return true;
      //   }
      //   else
      //   {
      //       return false;
      //   }
      // }

});
</script>

</html>
