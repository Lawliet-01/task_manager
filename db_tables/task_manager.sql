-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 30, 2019 at 08:11 PM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task_manager`
--

-- --------------------------------------------------------

--
-- Table structure for table `ActiveSessions`
--

CREATE TABLE `ActiveSessions` (
  `sessionid` varchar(50) NOT NULL,
  `userid` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ActiveSessions`
--

INSERT INTO `ActiveSessions` (`sessionid`, `userid`) VALUES
('135bae973102894a247abaaa23f5f8a1', '1621613031'),
('f59fc32dcc5917e2922b9980ab7d906d', 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `admin_notification`
--

CREATE TABLE `admin_notification` (
  `id` int(10) NOT NULL,
  `task_title` varchar(50) NOT NULL,
  `assigned_to` varchar(50) NOT NULL,
  `task_details` varchar(300) NOT NULL,
  `not_type` varchar(50) NOT NULL,
  `status` varchar(8) NOT NULL DEFAULT 'Pending',
  `date_time` timestamp(6) NOT NULL DEFAULT current_timestamp(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_notification`
--

INSERT INTO `admin_notification` (`id`, `task_title`, `assigned_to`, `task_details`, `not_type`, `status`, `date_time`) VALUES
(2, 'New', 'Deepak', 'Php dev', 'Follow Up Date', 'Seen', '2018-03-11 06:47:07.210291'),
(3, 'New', 'Ankush', 'php dev', 'Task Completed', 'Seen', '2018-03-11 07:07:54.135830'),
(4, 'New', 'Ankush', 'php dev', 'Task Completed', 'Seen', '2018-03-11 07:55:25.981933'),
(5, 'dummy task', 'Deepak', 'dummy task details', 'Sub task Completed', 'Seen', '2018-03-11 08:20:36.260087'),
(6, 'dummy task', 'Deepak', 'dummy task details', 'Sub task Completed', 'Seen', '2018-03-11 08:23:20.394698'),
(7, 'dummy task', 'Deepak', 'dummy task details', 'Task Completed', 'Seen', '2018-03-11 08:39:38.863508'),
(8, 'New', 'Ankush', 'php dev', 'Follow Up Date', 'Seen', '2018-03-11 09:50:16.500721'),
(9, 'dummy task', 'Deepak', 'dummy task details', 'Follow Up Date', 'Seen', '2018-03-11 09:50:16.576446'),
(10, 'New', 'Ankush', 'php dev', 'Follow Up Date', 'Seen', '2018-03-11 09:52:03.230417'),
(11, 'dummy task', 'Deepak', 'dummy task details', 'Follow Up Date', 'Seen', '2018-03-11 09:52:03.291719'),
(12, 'New', 'Ankush', 'php dev', 'Follow Up Date', 'Seen', '2018-03-11 11:26:10.627531'),
(13, 'dummy task', 'Deepak', 'dummy task details', 'Follow Up Date', 'Seen', '2018-03-11 11:26:10.677398'),
(14, 'Project x', 'Rohan', 'Debugging the newly created app that needs to be out soon', 'Sub task Completed', 'Seen', '2018-03-11 11:34:11.557280'),
(15, 'dummy task', 'Deepak', 'dummy task details', 'Task Completed', 'Seen', '2019-07-30 15:04:17.144622'),
(16, 'A', 'Parul', 'Testing', 'Task Completed', 'Seen', '2019-07-30 17:08:54.077971'),
(17, 'Project x', 'Rohan', 'Debugging the newly created app that needs to be out soon', 'Follow Up Date', 'Seen', '2019-07-30 17:44:35.723315'),
(18, 'A', 'Parul', 'Testing', 'Sub task Completed', 'Seen', '2019-07-30 17:55:32.966335'),
(19, 'A', 'Parul', 'Testing', 'Sub task Completed', 'Pending', '2019-07-30 17:56:33.398372'),
(20, 'A', 'Parul', 'Testing', 'Task Completed', 'Seen', '2019-07-30 17:56:38.759555');

-- --------------------------------------------------------

--
-- Table structure for table `all_task`
--

CREATE TABLE `all_task` (
  `task_id` int(10) NOT NULL,
  `task_title` varchar(50) NOT NULL,
  `task_type` varchar(20) NOT NULL,
  `task_details` varchar(300) NOT NULL,
  `assign_to` varchar(15) NOT NULL,
  `assign_date` date NOT NULL,
  `estimated_date` date NOT NULL,
  `Follow_up` date NOT NULL,
  `status` varchar(15) NOT NULL,
  `completed_on` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `all_task`
--

INSERT INTO `all_task` (`task_id`, `task_title`, `task_type`, `task_details`, `assign_to`, `assign_date`, `estimated_date`, `Follow_up`, `status`, `completed_on`) VALUES
(21, 'New', 'Development', 'php dev', 'Ankush', '2018-03-11', '2018-03-13', '2018-03-11', 'Completed', '2018-03-17'),
(22, 'dummy task', 'dummy', 'dummy task details', 'Deepak', '2018-03-11', '2018-03-14', '2018-03-11', 'Completed', '2019-07-30'),
(23, 'Project x', 'Debuggin', 'Debugging the newly created app that needs to be out soon', 'Rohan', '2018-02-25', '2018-05-25', '2019-07-30', 'Pending', '0000-00-00'),
(24, 'A', 'New Task Type', 'Testing', 'Parul', '2019-07-30', '2019-07-31', '2019-07-31', 'Completed', '2019-07-30');

-- --------------------------------------------------------

--
-- Table structure for table `assigned_tab`
--

CREATE TABLE `assigned_tab` (
  `assigned_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assigned_tab`
--

INSERT INTO `assigned_tab` (`assigned_name`) VALUES
('Ankit'),
('Ankush'),
('Deepak'),
('dummy'),
('Parul'),
('Rohan'),
('sad');

-- --------------------------------------------------------

--
-- Table structure for table `logindetail`
--

CREATE TABLE `logindetail` (
  `userid` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL DEFAULT '',
  `pass` varchar(32) NOT NULL,
  `type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logindetail`
--

INSERT INTO `logindetail` (`userid`, `name`, `email`, `pass`, `type`) VALUES
('1621613000', 'dummy', '', 'dXNlcg==', 'User'),
('1621613007', 'Ankit', '', 'aWltdEAxMjM=', 'User'),
('1621613010', 'Ankush', 'ankit29996@gmail.com', 'aWltdEAxMjM=', 'User'),
('1621613018', 'Deepak', '', 'aWltdEAxMjM=', 'User'),
('1621613031', 'Parul', '', 'aWltdEAxMjM=', 'User'),
('23456', 'Rohan', '', 'aWltdEAxMjM=', 'User'),
('Test', 'Admin', 'test@gmail.com', 'YWRtaW4=', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `rights`
--

CREATE TABLE `rights` (
  `id` int(2) NOT NULL,
  `pagename` varchar(25) NOT NULL,
  `function` varchar(20) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'Admin'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rights`
--

INSERT INTO `rights` (`id`, `pagename`, `function`, `type`) VALUES
(1, 'assign_task', 'ASSIGN TASK', 'Admin'),
(2, 'sub_task', 'ADD SUB TASK', 'Admin'),
(3, 'update_task', 'UPDATE TASK', 'Admin'),
(4, 'task_reports', 'VIEW TASK DETAIL', 'Admin'),
(5, 'add_new_user', 'ADD EMPLOYEE', 'Admin'),
(6, 'alloted_task', 'ALLOTED TASKS', 'User'),
(7, 'changepass', 'UPDATE PASSWORD', 'Both'),
(8, 'changeemail', 'UPDATE EMAIL', 'Both');

-- --------------------------------------------------------

--
-- Table structure for table `sub_task`
--

CREATE TABLE `sub_task` (
  `sub_id` int(10) NOT NULL,
  `task_title` varchar(50) NOT NULL,
  `st_title` varchar(100) NOT NULL,
  `st_detail` varchar(300) NOT NULL,
  `completed_on` date NOT NULL DEFAULT '0000-00-00',
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_task`
--

INSERT INTO `sub_task` (`sub_id`, `task_title`, `st_title`, `st_detail`, `completed_on`, `status`) VALUES
(1, 'dummy task', 'sub 1', 'dummy sub 1', '2018-03-11', 'Completed'),
(2, 'dummy task', 'sub 2', 'dummy sub 2', '2018-03-11', 'Completed'),
(3, 'Project x', 'Testing ', 'Ruling  out bugs', '2018-04-11', 'Completed'),
(4, 'A', 'gg', 'hh', '2019-07-30', 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `task_type`
--

CREATE TABLE `task_type` (
  `t_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task_type`
--

INSERT INTO `task_type` (`t_type`) VALUES
('Debuggin'),
('Development'),
('dummy'),
('New Task Type'),
('Random Check');

-- --------------------------------------------------------

--
-- Table structure for table `user_notes`
--

CREATE TABLE `user_notes` (
  `id` int(11) NOT NULL,
  `task_title` varchar(50) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_notes`
--

INSERT INTO `user_notes` (`id`, `task_title`, `note`, `date`) VALUES
(1, 'A', '<p>Testing notes</p>\n\n<p>&nbsp;</p>\n', '2019-07-30'),
(2, 'A', '<p>completed</p>\n\n<p>&nbsp;</p>\n', '2019-07-30');

-- --------------------------------------------------------

--
-- Table structure for table `user_notification`
--

CREATE TABLE `user_notification` (
  `id` int(10) NOT NULL,
  `task_title` varchar(50) NOT NULL,
  `assigned_to` varchar(50) NOT NULL,
  `task_details` varchar(300) NOT NULL,
  `not_type` varchar(50) NOT NULL,
  `status` varchar(8) NOT NULL DEFAULT 'Pending',
  `date_time` timestamp(6) NOT NULL DEFAULT current_timestamp(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_notification`
--

INSERT INTO `user_notification` (`id`, `task_title`, `assigned_to`, `task_details`, `not_type`, `status`, `date_time`) VALUES
(35, 'New', 'Ankush', 'php dev', 'New Task', 'Seen', '2018-03-11 07:07:00.464304'),
(41, 'dummy task', 'Deepak', 'dummy task details', 'Changes Required', 'Seen', '2018-03-11 07:58:42.310092'),
(42, 'dummy task', 'Deepak', 'dummy task details', 'Changes Required', 'Seen', '2018-03-11 08:22:45.018187'),
(43, 'dummy task', 'Deepak', 'dummy task details', 'Changes Required', 'Seen', '2018-03-11 08:22:47.371582'),
(44, 'dummy task', 'Deepak', 'dummy task details', 'Change In Follow Up', 'Seen', '2018-03-11 08:39:17.455639'),
(45, 'dummy task', 'Deepak', 'dummy task details', 'Follow Up Date', 'Seen', '2018-03-11 08:39:20.520433'),
(46, 'dummy task', 'Deepak', 'dummy task details', 'Changes Required', 'Seen', '2018-03-11 08:39:55.930669'),
(47, 'New', 'Ankush', 'php dev', 'Follow Up Date', 'Pending', '2018-03-11 09:37:28.507889'),
(48, 'New', 'Ankush', 'php dev', 'Follow Up Date', 'Pending', '2018-03-11 09:42:35.560574'),
(49, 'Project x', 'Rohan', 'Debugging the newly created app that needs to be out soon', 'New Task', 'Seen', '2018-03-11 11:30:15.565142'),
(50, 'Project x', 'Rohan', 'Debugging the newly created app that needs to be out soon', 'Change In Follow Up', 'Pending', '2018-03-11 11:35:54.341359'),
(51, 'New', 'Ankush', 'php dev', 'Follow Up Date', 'Pending', '2018-03-11 16:55:06.351792'),
(52, 'dummy task', 'Deepak', 'dummy task details', 'Follow Up Date', 'Seen', '2018-03-11 16:55:30.523577'),
(55, 'A', 'Parul', 'Testing', 'New Task', 'Seen', '2019-07-30 17:05:55.613446'),
(56, 'A', 'Parul', 'Testing', 'New Task', 'Pending', '2019-07-30 17:07:43.851048'),
(57, 'A', 'Parul', 'Testing', 'Changes Required', 'Seen', '2019-07-30 17:09:45.469068'),
(58, 'Project x', 'Rohan', 'Debugging the newly created app that needs to be out soon', 'Change In Follow Up', 'Pending', '2019-07-30 17:17:32.705969'),
(59, 'Project x', 'Rohan', 'Debugging the newly created app that needs to be out soon', 'Change In Follow Up', 'Pending', '2019-07-30 17:44:20.752915'),
(60, 'A', 'Parul', 'Testing', 'Changes Required', 'Seen', '2019-07-30 17:56:17.889263');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ActiveSessions`
--
ALTER TABLE `ActiveSessions`
  ADD PRIMARY KEY (`sessionid`,`userid`);

--
-- Indexes for table `admin_notification`
--
ALTER TABLE `admin_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `all_task`
--
ALTER TABLE `all_task`
  ADD PRIMARY KEY (`task_id`),
  ADD UNIQUE KEY `task_title` (`task_title`);

--
-- Indexes for table `assigned_tab`
--
ALTER TABLE `assigned_tab`
  ADD PRIMARY KEY (`assigned_name`);

--
-- Indexes for table `logindetail`
--
ALTER TABLE `logindetail`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `rights`
--
ALTER TABLE `rights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_task`
--
ALTER TABLE `sub_task`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `task_type`
--
ALTER TABLE `task_type`
  ADD PRIMARY KEY (`t_type`);

--
-- Indexes for table `user_notes`
--
ALTER TABLE `user_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_notification`
--
ALTER TABLE `user_notification`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_notification`
--
ALTER TABLE `admin_notification`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `all_task`
--
ALTER TABLE `all_task`
  MODIFY `task_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `rights`
--
ALTER TABLE `rights`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sub_task`
--
ALTER TABLE `sub_task`
  MODIFY `sub_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_notes`
--
ALTER TABLE `user_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_notification`
--
ALTER TABLE `user_notification`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
